﻿using UnityEngine;
using System.Collections;

public class TrailerCamera : MonoBehaviour {

	float speed = 3f;
	float logoTimer = 5f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		logoTimer -= Time.deltaTime;
		if (logoTimer < 0 && transform.position.y > 6.2f)
			transform.Translate(0, -speed * Time.deltaTime, 0);

		if (Input.GetMouseButtonDown(0)) {
			GameObject.Find ("Grubbus").GetComponent<Animator>().SetTrigger("greedy");
			GameObject.Find ("Grubby").GetComponent<Animator>().SetTrigger("greedy");
		}
	}

}
