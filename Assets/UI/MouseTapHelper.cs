﻿using UnityEngine;
using System.Collections;

public class MouseTapHelper : MonoBehaviour {
	
	private Tutor tutorScript;

	// Use this for initialization
	void Start () {
		tutorScript = GameObject.Find ("Tutor").GetComponent<Tutor>();	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseDown() {
		tutorScript.clickedHelper = true;
	}
}
