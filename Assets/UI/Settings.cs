﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Settings : MonoBehaviour {
	
	public Sprite lockedSprite;

	private Toggle music;
	private Toggle sfx;
	private Toggle tutorial;
	private Toggle enlightened;
	private GameObject difficulty;

	bool isRestoring;
	bool isSettingDifficulty;

	// Use this for initialization
	void Start () {
		music = GameObject.Find ("Music").GetComponentInChildren<Toggle>();
		sfx = GameObject.Find ("SoundFX").GetComponentInChildren<Toggle>();
		tutorial = GameObject.Find ("Tutorial").GetComponentInChildren<Toggle>();
		enlightened = GameObject.Find ("Enlightened").GetComponentInChildren<Toggle>();

		// isRestoring used to prevent button toggle SFX on start
		isRestoring = true;
		RestoreToggles();
		RestoreDifficulty();
		isRestoring = false;

		LockEnlightened();
		LockLevels();

	}

	void RestoreToggles() {
		music.isOn = SecurePlayerPrefs.GetBool(SavedData.EnabledMusic);
		sfx.isOn = SecurePlayerPrefs.GetBool(SavedData.EnabledSFX);
		tutorial.isOn = SecurePlayerPrefs.GetBool(SavedData.EnabledTutorial);
		enlightened.isOn = SecurePlayerPrefs.GetBool(SavedData.EnabledEnlightened);
	}

	void RestoreDifficulty() {
		// Find old difficulty from comparing initScore, and turn on the last selected level
		difficulty = GameObject.Find ("DifficultyToggle");
		for (int i = 0; i < difficulty.transform.childCount - 1; i++) {
			int initScore = SecurePlayerPrefs.GetInt(SavedData.InitScore);
			if (difficulty.transform.GetChild(i).GetComponentInChildren<Text>().text == initScore.ToString()) {
				difficulty.transform.GetChild(i).GetComponent<Toggle>().isOn = true;
			}
		}
	}

	void LockLevels() {
		for (int i = 0; i < SavedData.UnlockedLevel.Length; i++) {
			bool unlockedCurrLevel = SecurePlayerPrefs.GetBool(SavedData.UnlockedLevel[i]);
			if (!unlockedCurrLevel) {
				difficulty.transform.GetChild(i).GetComponentInChildren<Image>().sprite = lockedSprite;
				difficulty.transform.GetChild(i).GetComponent<Toggle>().interactable = false;
			}
		}
	}

	void LockEnlightened() {
		bool unlockedEnlightened = SecurePlayerPrefs.GetBool(SavedData.UnlockedEnlightened);
		if (!unlockedEnlightened) {
			enlightened.gameObject.GetComponentInChildren<Image>().sprite = lockedSprite;
			enlightened.interactable = false;
		}
	}

	public void ToggleMusic() {
		PlayButtonSound ();
		AudioManager.instance.ToggleBGM(music.isOn);
		SecurePlayerPrefs.SetBool(SavedData.EnabledMusic, music.isOn);
	}
	
	public void ToggleSFX() {
		AudioManager.instance.ToggleSFX(sfx.isOn);
		SecurePlayerPrefs.SetBool(SavedData.EnabledSFX, sfx.isOn);
		PlayButtonSound ();
	}
	
	public void ToggleTutorial() {
		PlayButtonSound ();
		SecurePlayerPrefs.SetBool(SavedData.EnabledTutorial, tutorial.isOn);
		if (Application.loadedLevelName == "Menu")
			FindObjectOfType<Menu>().RestoreStoreButton();
	}

	public void ToggleEnlightened() {
		PlayButtonSound ();
		SecurePlayerPrefs.SetBool(SavedData.EnabledEnlightened, enlightened.isOn);

		if (!isRestoring)
			GoogleAnalyticsV3.instance.LogEvent("Conversion", "Set Enlightened", enlightened.isOn.ToString(), 1);
	}
	
	public void SetDifficulty() {
		isSettingDifficulty = true;
		for (int i = 0; i < difficulty.transform.childCount - 1; i++) {
			if (difficulty.transform.GetChild (i).GetComponent<Toggle>().isOn) {
				PlayButtonSound ();
				int newInitScore = int.Parse(difficulty.transform.GetChild(i).GetComponentInChildren<Text>().text);
				SecurePlayerPrefs.SetInt(SavedData.InitScore, newInitScore);
			}
		}
		isSettingDifficulty = false;
	}

	public void Enter() {
		PlayButtonSound ();
		GetComponent<CanvasFader>().FadeIn();
		
		GoogleAnalyticsV3.instance.LogScreen("Settings");
	}

	public void Exit() {
		PlayButtonSound();
		GetComponent<CanvasFader>().FadeOut();

		GoogleAnalyticsV3.instance.LogScreen(Application.loadedLevelName);
	}

	void PlayButtonSound () {
		// Don't play is clip is already playing AND difficulty is being set
		bool isPlaying = AudioManager.instance.IsPlayingClip(AudioManager.instance.button);
		bool recentlySetDifficulty = isPlaying && isSettingDifficulty;
		if (!isRestoring && !recentlySetDifficulty)
			AudioManager.instance.PlayClip(AudioManager.instance.button);
	}
}
