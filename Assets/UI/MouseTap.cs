﻿using UnityEngine;
using System.Collections;

public class MouseTap : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy (gameObject, 0.75f);
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, Generator.instance.speed);
	}

	void Update() {
		transform.Translate(0, Generator.instance.speed * Time.deltaTime, 0);
	}
}
