﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class DialogBox : MonoBehaviour {

	GameObject confirmationDialog;
	GameObject notificationDialog;

	// Singleton
	private DialogBox() {}
	private static DialogBox _instance;
	public static DialogBox instance {
		get {
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<DialogBox>();
			}
			return _instance;
		}
	}

	// Use this for initialization
	void Start () {
		confirmationDialog = transform.FindChild("ConfirmationDialog").gameObject;
		notificationDialog = transform.FindChild("NotificationDialog").gameObject;
	}
	
	public void ShowConfirmation(string dialogText, Action yesMethod) {
		// Assign new text and new onClick listener for 'YES' button
		confirmationDialog.GetComponentInChildren<Text>().text = dialogText;
		Button yesButton = confirmationDialog.transform.FindChild("YesButton").gameObject.GetComponent<Button>();
		yesButton.onClick.RemoveAllListeners();
		yesButton.onClick.AddListener(() => yesMethod());
		yesButton.onClick.AddListener(() => DismissConfirmation());
		confirmationDialog.GetComponent<CanvasFader>().FadeIn();
	}

	public void DismissConfirmation() {
		confirmationDialog.GetComponent<CanvasFader>().FadeOut();
	}

	public void ShowNotification(string dialogText) {
		notificationDialog.GetComponentInChildren<Text>().text = dialogText;
		notificationDialog.GetComponent<CanvasFader>().FadeIn();
	}

	public void DismissNotification() {
		notificationDialog.GetComponent<CanvasFader>().FadeOut();
	}
}
