﻿using UnityEngine;
using System.Collections;

public class BonusText : MonoBehaviour {

	bool isShowing = false;
	float speed = 0.5f;
	float fadeOutTimer = 0.5f;
	CanvasFader canvasFader;

	void Start () {
		canvasFader = GetComponent<CanvasFader>();
	}
	
	// Update is called once per frame
	void Update () {
		if (isShowing) {
			MoveUp ();
			fadeOutTimer -= Time.deltaTime;
			if (ShouldFadeOut()) {
				isShowing = false;
				GetComponent<CanvasFader>().FadeOut();
			}
		}

		else if (canvasFader.IsDoneFadeOut()) {
			Destroy(gameObject);
		}
	}

	void MoveUp() {
		transform.position += Vector3.up * speed * Time.deltaTime;
	}

	bool ShouldFadeOut() {
		return fadeOutTimer < 0;
	}

	public void Show(Vector2 hitPos) {
		transform.position = hitPos;
		isShowing = true;
		GetComponent<CanvasFader>().FadeIn();
	}
}
