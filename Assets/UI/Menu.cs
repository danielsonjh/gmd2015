﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using Soomla.Profile;
using Soomla.Store;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class Menu : MonoBehaviour {

	GameObject storeButton;

	int maxDifficulty = 300;

	private Animator grubby_anim;
	private Animator grubbus_anim;
	
	private GameObject deathOverlay;
	private Score scoreScript;
	
	private float fadeTime = 0.25f;
	
	string confuciusQuote = "To go beyond is as wrong as to fall short.";
	string[] deathTips = new string[7] {
		"Grubby can shoot silk behind him",
		"Silk will detach when Grubby is aligned to the rock",
		"Grubby can only have one silk at a time",
		"Shooting down canes gives a bonus but destroys the silk",
		"Canes are always thrown to Grubby's current location",
		"Leaves tend to spawn far away from Grubby",
		"The difficulty can be changed in settings"
		};

	// Use this for initialization
	void Start () {
		if (gameObject.name == "Menu") {
			grubby_anim = GameObject.Find ("Grubby").GetComponent<Animator>();
			grubbus_anim = GameObject.Find ("Grubbus").GetComponent<Animator>();

			storeButton = GameObject.Find ("Store");
			RestoreStoreButton();
		}
		
		else if (gameObject.name == "HUD") {
			scoreScript = GameObject.Find("Score").GetComponent<Score>();
			scoreScript.highScoreText = GameObject.Find ("HighScore");
			scoreScript.newHighScoreImage = GameObject.Find ("New");
			scoreScript.newHighScoreImage.SetActive(false);
			deathOverlay = GameObject.Find ("DeathOverlay");
			deathOverlay.SetActive (false);
		}

		else if (gameObject.name == "ShopMenu") {
			ChangeToggleAdsText();
		}
	}

	void ChangeToggleAdsText() {
		Text theText = GameObject.Find ("ToggleAds").GetComponentInChildren<Text>();
		bool enabledAds = SecurePlayerPrefs.GetBool(SavedData.EnabledAds);
		if (enabledAds) {
			theText.text = "Disable Ads";
		}
		else {
			theText.text = "Enable Ads";
		}
		 
	}

	public void RestoreStoreButton() {
		bool enabledTutorial = SecurePlayerPrefs.GetBool(SavedData.EnabledTutorial);
		storeButton.SetActive(!enabledTutorial);
	}

	/** SOCIAL ACTIONS **/

	public void ConfirmFBUpload () {		
		PlayButtonSound ();
		DialogBox.instance.ShowConfirmation("Would you like to share your high score on Facebook?", FBUploadScreenshot);
	}

	public void ConfirmTwitterUpload () {
		PlayButtonSound ();
		DialogBox.instance.ShowConfirmation("Would you like to share your high score on Twitter?", TwitterUploadScreenshot);
	}

	void FBUploadScreenshot () {
		ProfileLogin (Provider.FACEBOOK);
		StartCoroutine(UploadScreenshot(Provider.FACEBOOK));
	}
	
	void TwitterUploadScreenshot () {
		ProfileLogin (Provider.TWITTER);
		StartCoroutine(UploadScreenshot(Provider.TWITTER));
	}
	
	void ProfileLogin(Provider provider) {
		if (SoomlaProfile.IsLoggedIn(provider))
			return;
		SoomlaProfile.Login(provider);
	}

	IEnumerator UploadScreenshot(Provider provider) {
		// Need Coroutine to wait for user to finish logging in before trying to upload image
		int waitFrameCount = 50;
		while (!SoomlaProfile.IsLoggedIn(provider)) {
			if (waitFrameCount > 0) {
				waitFrameCount--;
				yield return new WaitForEndOfFrame();
			}
			else {
				return false;
			}
		}
		// Temporarily remove dialog box, then take screenshot
		Canvas dialogBoxCanvas = DialogBox.instance.gameObject.GetComponent<Canvas>();
		dialogBoxCanvas.enabled = false;
		yield return new WaitForEndOfFrame();
		var width = Screen.width;
		var height = Screen.height;
		var tex = new Texture2D(width, height, TextureFormat.ARGB32, false);
		tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		tex.Apply();
		dialogBoxCanvas.enabled = true;

		SoomlaProfile.UploadImage(
			provider,
			"Try to beat my high score on Greedy Grubbies!",
			"screenshot.jpg",
			tex,                          // The image as a texture
			"",                           // Payload
			null
			);
	}

	public void OpenLeaderboards() {
		PlayButtonSound ();
		Social.localUser.Authenticate((bool success) => {
			if (success) {
				scoreScript.ReportScoreToLeaderboards();
				Social.ShowLeaderboardUI();

				GoogleAnalyticsV3.instance.LogEvent("Social", "Open Leaderboards", "Google Play Games", 1);
			}
		});
	}
	
	public void OpenAchievements() {
		PlayButtonSound ();
		Social.localUser.Authenticate((bool success) => {
			if (success) {
				Social.ShowAchievementsUI();

				GoogleAnalyticsV3.instance.LogEvent("Social", "Open Achievements", "Google Play Games", 1);
			}
		});
	}
	
	/** STORE ACTIONS **/

	public void Donate() {
		PlayButtonSound ();
		SoomlaStore.BuyMarketItem("donate1", "");

		GoogleAnalyticsV3.instance.LogEvent("Conversion", "Donate", "Clicked", 1);
	}

	public void ToggleAds() {
		PlayButtonSound ();
		bool enabledAds = SecurePlayerPrefs.GetBool(SavedData.EnabledAds);
		enabledAds = !enabledAds;
		SecurePlayerPrefs.SetBool(SavedData.EnabledAds, enabledAds);
		ChangeToggleAdsText();
		if (enabledAds) {
			SecurePlayerPrefs.SetBool(SavedData.UnlockedEnlightened, true);
			SecurePlayerPrefs.SetBool(SavedData.EnabledEnlightened, true);
			Achievements.Award(Achievements.EnlightenedGrubby);
			DialogBox.instance.ShowNotification("Thank you for enabling ads! " +
				"Grubby is enlightened while ads are enabled. The effect can be toggled in Settings.");

			GoogleAnalyticsV3.instance.LogEvent("Conversion", "Ads", "Enabled", 1);
		}
		else {
			int donationBalance = StoreInventory.GetItemBalance(StoreAssets.DONATE1.ItemId);
			if (donationBalance == 0) {
				SecurePlayerPrefs.SetBool(SavedData.UnlockedEnlightened, false);
				SecurePlayerPrefs.SetBool(SavedData.EnabledEnlightened, false);
				DialogBox.instance.ShowNotification("Disabled ads! Grubby is no longer enlightened.");
			}
			else {
				DialogBox.instance.ShowNotification("Disabled ads!");
			}

			GoogleAnalyticsV3.instance.LogEvent("Conversion", "Ads", "Disabled", 1);
		}
	}

	public void RateApp() {
		PlayButtonSound ();
		Application.OpenURL("https://play.google.com/store/apps/details?id=com.KungFuHippie.GMD2015");

		GoogleAnalyticsV3.instance.LogEvent("Conversion", "Rate", "Clicked", 1);
	}

	public void ToAbout() {
		PlayButtonSound ();
		FadeOut ();
		StartCoroutine(LoadLevelAfterDelay("About", fadeTime));
	}


	/** NAVIGATION ACTIONS **/

	public void PlayGame() {
		PlayButtonSound ();
		Time.timeScale = 1f;
		grubby_anim.SetTrigger("greedy");
		grubbus_anim.SetTrigger("greedy");
		GetComponent<CanvasFader>().FadeOut();
		
		bool enabledTutorial = SecurePlayerPrefs.GetBool(SavedData.EnabledTutorial);
		
		float startGameDelay = 3.25f;
		Invoke ("FadeOut", startGameDelay);
		if (enabledTutorial)
			StartCoroutine(LoadLevelAfterDelay("Tutorial", startGameDelay + fadeTime));
		else
			StartCoroutine(LoadLevelAfterDelay("Game", startGameDelay + fadeTime));
	}
	
	public void RestartGame() {
		PlayButtonSound ();
		Time.timeScale = 1f;
		FadeOut ();
		StartCoroutine(LoadLevelAfterDelay("Game", fadeTime));
	}
	
	public void RestartTutorial() {
		PlayButtonSound ();
		FadeOut ();
		StartCoroutine(LoadLevelAfterDelay("Tutorial", fadeTime));
	}
	
	public void ToMenu () {
		PlayButtonSound ();
		Time.timeScale = 1f;
		SoomlaStore.StopIabServiceInBg();	// In App Billing
		FadeOut ();
		StartCoroutine(LoadLevelAfterDelay("Menu", fadeTime));
	}
	
	public void ToShop () {
		PlayButtonSound ();
		if (Application.loadedLevelName == "Menu") {
			GetComponent<CanvasFader>().FadeOut();
			grubbus_anim.SetTrigger("generous");
			float animationDelay = 0.7f;
			Invoke ("FadeOut", animationDelay);
			StartCoroutine(LoadLevelAfterDelay("Shop", fadeTime + animationDelay));
		}
		else {
			FadeOut ();
			StartCoroutine(LoadLevelAfterDelay("Shop", fadeTime));
		}

		SoomlaStore.StartIabServiceInBg();	// In App Billing
	}
	
	public void EnableDeathOverlay() {
		deathOverlay.SetActive (true);
		Text tipText = GameObject.Find ("Tip").GetComponent<Text>();
		if (scoreScript.GetScore() > maxDifficulty) {
			tipText.text = confuciusQuote;
			tipText.fontStyle = FontStyle.Italic;
		}
		else {
			tipText.text = deathTips[Random.Range (0, deathTips.Length)];
			tipText.fontStyle = FontStyle.Normal;
		}
		scoreScript.ShowScore();
		StartCoroutine(FadeInAfterDelay (0.5f));

	}
	
	public void TogglePause () {
		PlayButtonSound ();
		bool wasPaused = Time.timeScale == 0;
		Image pauseImage = GameObject.Find ("Pause").transform.GetChild(0).GetComponent<Image>();
		if (wasPaused) {
			Time.timeScale = 1;
			pauseImage.enabled = true;
			AudioManager.instance.unpausedSnapshot.TransitionTo(0);
		}
		else {
			Time.timeScale = 0;
			pauseImage.enabled = false;
			AudioManager.instance.pausedSnapshot.TransitionTo(0);
		}
	}

	void PlayButtonSound () {
		AudioManager.instance.PlayClip(AudioManager.instance.button);
	}
	
	void FadeOut() {
		// Fade in the screen fader
		GameObject.Find ("ScreenFader").GetComponent<CanvasFader>().FadeIn ();
	}

	IEnumerator LoadLevelAfterDelay(string levelName, float delay) {
		yield return new WaitForSeconds(delay);
		Application.LoadLevel(levelName);
	}

	IEnumerator FadeInAfterDelay(float seconds) {
		yield return new WaitForSeconds(seconds);
		deathOverlay.GetComponent<CanvasFader>().FadeIn ();
	}
}
