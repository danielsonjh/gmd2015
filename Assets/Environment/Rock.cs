﻿using UnityEngine;
using System.Collections;

public class Rock : MonoBehaviour {

	public bool hasBonus;
	public Sprite[] sprites;
	
	private int frameCount = 0;
	private SpriteRenderer spriteRenderer;
	private GameObject bonusGameObject;
	private float boatLocY = 0;

	void Start () {
		if (Random.Range(0, 2) == 1) {
			transform.localScale = new Vector3(-1, 1, 1);
		}
			
		bonusGameObject = transform.GetChild(2).gameObject;
		spriteRenderer = GetComponentInChildren<SpriteRenderer>();
		
		GameObject boat = GameObject.Find ("Boat");
		if (boat != null) boatLocY = boat.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		frameCount++;
		if (frameCount < 1)
			return;
		frameCount = 0;
	
		if (transform.position.y < boatLocY) {
			spriteRenderer.sortingOrder = 19;
		}
		
		bonusGameObject.SetActive(hasBonus);
	}
}
