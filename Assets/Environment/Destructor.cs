﻿using UnityEngine;
using System.Collections;

public class Destructor : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnCollisionEnter2D(Collision2D coll) {
		coll.gameObject.SetActive(false);
	}
	
	void OnTriggerEnter2D(Collider2D coll) {
		coll.gameObject.SetActive(false);
	}
}
