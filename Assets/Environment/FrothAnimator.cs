﻿using UnityEngine;
using System.Collections;

public class FrothAnimator : MonoBehaviour {

//	private float maxDeltaPos = 0f;
	private float maxScale = 1.15f;
	private float minScale = 1.05f;
	
	private float omega = 1f * (2 * Mathf.PI);
	private float phaseShift;
	
	private float currAmp = 0;
	private float prevAmp = 0;
	
	private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
		int r = Random.Range(0, 2);
		if (r == 1)
			transform.localScale = new Vector2 (-transform.localScale.x, transform.localScale.y);
		phaseShift = Random.Range (0, 2 * Mathf.PI);
		
		spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

		currAmp = Mathf.Sin (omega * Time.time + phaseShift) * (maxScale - minScale)/2;
		float delta = currAmp - prevAmp;
		
		transform.localScale = new Vector2(
			Mathf.Clamp (transform.localScale.x + delta, minScale, maxScale),
			Mathf.Clamp (transform.localScale.y - delta, minScale, maxScale));

//		transform.position = new Vector2(
//			Mathf.Clamp (transform.position.x + Random.Range (-delta, delta), transform.position.x-maxDeltaPos, transform.position.x+maxDeltaPos),
//			Mathf.Clamp (transform.position.y + Random.Range (-delta, delta), transform.position.y-maxDeltaPos, transform.position.y+maxDeltaPos));
	
		float alpha = spriteRenderer.color.a;
		spriteRenderer.color = new Color(1, 1, 1, Mathf.Clamp (alpha - delta, 0.5f, 1f));
	
	
		prevAmp = currAmp;
	}
}
