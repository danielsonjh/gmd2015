﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ExtensionMethods;

public class Generator : MonoBehaviour {

	// Singleton
	private Generator() {}
	private static Generator _instance;
	public static Generator instance {
		get {
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<Generator>();
			}
			return _instance;
		}
	}

	private int frameCount = 0;
	
	private bool isTutorial;

	private ObjectPooler rockPool;
	private ObjectPooler wavePool;

	public float speed = -2.5f;
	public bool moving = true;

	private GameObject boat;
	private bool boatIsMoving;
	private float currBoatPos;
	private bool isCurrBoatLoc;
	
	private float[] locationsArray;
	private List<float> locations;
	private float maxLocNoise = 0.5f;
	private float locNoise;
	
	private Vector2 castOrigin;
	private float castRadius = 2.5f;
	private float maxRadiusNoise = 1f;
	private float[] radiusNoise;
	private bool isNearRock;
	
	private float nextWaveTime;
	private float nextBonusTime;
	private float bonusCooldown = 3f;


	public void ChangeDifficulty (float newSpeed, float newCastRadius) {
		speed = newSpeed;
		castRadius =  newCastRadius;
		//maxRadiusNoise = Mathf.Abs (speed);
		GameObject[] movingObjects = GameObject.FindGameObjectsWithTag("Moving");
		foreach (GameObject movingObject in movingObjects) {
			movingObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
		}
	}

	void Start () {
		rockPool = GameObject.Find ("RockPool").GetComponent<ObjectPooler>();
		wavePool = GameObject.Find ("WavePool").GetComponent<ObjectPooler>();
		nextWaveTime = GetNextWaveTime();
		nextBonusTime = bonusCooldown;
		boat = FindObjectOfType<Boat>().gameObject;
		
		locationsArray = new float[7] {0.5f, 1.5f, 2.5f, 3.5f, 4.5f, 5.5f, 6.5f};
		radiusNoise = new float[7];
		
		isTutorial = Application.loadedLevelName == "Tutorial";
	}
	
	void Update () {
		// Run garbage collection
		if (Time.frameCount % 30 == 0)
		{
			System.GC.Collect();
		}
	
		nextBonusTime -= Time.deltaTime;
		if (moving)
			GenerateWave ();
	
		frameCount++;
		if (frameCount < 2)
			return;
		frameCount = 0;

		// Reset locations
		locations = new List<float>(locationsArray);
	
		boatIsMoving = boat.GetComponent<Rigidbody2D>().velocity != Vector2.zero;
		currBoatPos = boat.transform.position.x;
		// If boat is not moving
		if (!boatIsMoving) {
			// Check if there is a clear vertical path between the boat and the top of the screen
			Vector2 rayStart = new Vector2(currBoatPos, transform.position.y);	
			RaycastHit2D boatHit = Physics2D.Raycast (rayStart, new Vector2(0, -1));
			// If the boat has a clear path in the screen, replace the spawn locations with just the boat location
			// so that a rock will appear to interrupt that path
			if (boatHit.collider.name == "Boat") {
				locations.Clear();
				locations.Add (currBoatPos);
			}
		}
		
		if (moving) {
			locations.Shuffle();
			foreach (float loc in locations) {
				int i = locations.IndexOf(loc);

				// No rocks for tutorial
				if (isTutorial) continue;
			
				// Assume no rocks are nearby
				isNearRock = false;
				castOrigin = new Vector2(loc, transform.position.y);
				// Only overwrite radiusNoise if a rock was spawned for the slot in the last frame
				if (radiusNoise[i] == 0)
					radiusNoise[i] = Random.Range (0.1f, maxRadiusNoise);
				
				// Check if rocks are nearby
				if (Physics2D.CircleCast (castOrigin, castRadius + radiusNoise[i], Vector2.up, 0f).collider != null ||
					Physics2D.Linecast (castOrigin, castOrigin + Vector2.up).collider != null)
					isNearRock = true;
				
				// Spawn rocks if no rocks nearby
				if (!isNearRock) {
					radiusNoise[i] = 0;
				
					// No spawn noise for rocks that spawn in front of the boat
					if (loc == currBoatPos)
						locNoise = 0;
					else
						locNoise = Random.Range(-maxLocNoise, maxLocNoise);
						
					// Make the rock and initialize position and velocity
					GameObject clone = rockPool.GetPooledObject();
					clone.SetActive(true);
					clone.GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
					clone.transform.position = new Vector2 (
						Mathf.Clamp (transform.position.x + loc + locNoise, 0.5f, 6.5f), transform.position.y);
						
					// Set sprite
					Rock rockScript = clone.GetComponent<Rock>();
					clone.transform.GetChild (0).GetComponent<SpriteRenderer>().sprite = // Change RockSprite's sprite
						rockScript.sprites[Random.Range(0,4)];
						
					// Set bonus
					rockScript.hasBonus = false;	// Stop random bonuses from spawning...
					if (nextBonusTime < 0 && Mathf.Abs (clone.transform.position.x - currBoatPos) > 3.5f) {
						rockScript.hasBonus = true;
						nextBonusTime = bonusCooldown;
					}
					
				}
			}
		}
	}
	
	void GenerateWave () {
		if (nextWaveTime > 0)
			nextWaveTime -= Time.deltaTime;
		
		else {
			nextWaveTime = GetNextWaveTime();
			GameObject clone = wavePool.GetPooledObject();
			clone.SetActive(true);
			clone.transform.GetComponent<SpriteRenderer>().sprite = // Change wave's sprite
				clone.GetComponent<WaveAnimator>().sprites[Random.Range(0,3)];
			clone.GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
			clone.transform.position = new Vector2 (gameObject.transform.position.x + 3.5f, gameObject.transform.position.y);
		}
	}
	
	
	float GetNextWaveTime () {
		return Mathf.Abs (3 / speed);
	}
	
	public void StopMovement () {
		GameObject[] movingObjects = GameObject.FindGameObjectsWithTag("Moving");
		foreach (GameObject movingObject in movingObjects) {
			movingObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		}
		moving = false;
	}
}
