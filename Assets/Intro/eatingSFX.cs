﻿using UnityEngine;
using System.Collections;

public class eatingSFX : MonoBehaviour {

	public AudioClip eating;

	// Use this for initialization
	void Awake () {
		GetComponent<AudioSource>().clip = eating;
		GetComponent<AudioSource>().Play ();
	}
}
