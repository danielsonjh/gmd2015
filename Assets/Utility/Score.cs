﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class Score : MonoBehaviour {
	string LEADERBOARD_ID = "CgkIg9u6lZMTEAIQAg";

	int score = 0;
	int numCanes = 0;
	int numLeaves = 0;

	int frameCount = 0;
	int fixedFramesPerScore = 25;
	int scorePerBonus = 5;

	int scorePerLevel = 50;
	int numGamesToUnlockLevel = 3;
	int highestInitScore = 200;
	float newSpeed;
	bool isTutorial;
	
	GameObject unlockPanel;
	Grubbus grubbusScript;
	Character character;

	public bool updating = true;
	
	public GameObject highScoreText;
	public GameObject newHighScoreImage;


	// Use this for initialization
	void Start () {
		if (Application.loadedLevelName == "Game") {
			unlockPanel = GameObject.Find ("Unlock");
			grubbusScript = GameObject.Find ("Grubbus").GetComponent<Grubbus>();
			// Load initial score
			score = SecurePlayerPrefs.GetInt(SavedData.InitScore);
		}
		character = GameObject.Find ("Boat").GetComponentInChildren<Character>();
		isTutorial = Application.loadedLevelName == "Tutorial";
	}
	

	void FixedUpdate () {
		if (updating) {
			frameCount++;
		 	if (frameCount >= fixedFramesPerScore) {
				frameCount = 0;
				score += 1;
				SetDifficulty();
			}
			gameObject.GetComponent<Text>().text = score.ToString();
		}
	}
	
	public void ShowScore() {
		updating = false;
		UpdateHighScore();
		ShowHighScore();
		UnlockLevels();
		if (Social.localUser.authenticated) {
			ReportScoreToLeaderboards();
			ReportAchievements();	// Must be after UnlockLevels since one achievement is for unlocking all levels
		}
	}

	void UpdateHighScore() {
		int prevHighScore = SecurePlayerPrefs.GetInt(SavedData.HighScore);
		if (score > prevHighScore) {
			SecurePlayerPrefs.SetInt(SavedData.HighScore, score);
			newHighScoreImage.SetActive(true);
		}
	}

	void ShowHighScore() {
		int currHighScore = SecurePlayerPrefs.GetInt(SavedData.HighScore);
		highScoreText.GetComponent<Text>().text += currHighScore.ToString();
	}

	void UnlockLevels() {
		int nextLevelUnlockCounter = SecurePlayerPrefs.GetInt(SavedData.NextLevelUnlockCounter);
		int initScore = SecurePlayerPrefs.GetInt(SavedData.InitScore);

		// Find the next unlock, and set the counter
		int nextLevelToUnlock = -1;
		for (int i = 0; i < SavedData.UnlockedLevel.Length; i++) {
			bool unlockedCurrLevel = SecurePlayerPrefs.GetBool(SavedData.UnlockedLevel[i]);
			if (!unlockedCurrLevel) {
				nextLevelToUnlock = i;
				break;
			}
		}

		// If there are still levels to unlock
		if (nextLevelToUnlock != -1) {
			// Setup information text for unlocking next level (50 higher than current initScore)
			unlockPanel.transform.GetChild(0).GetComponent<Text>().text = (initScore + scorePerLevel).ToString() + "+";
			// Above unlock score
			if (score >= nextLevelToUnlock * scorePerLevel) {
				nextLevelUnlockCounter++;
				
				// Set unlock leaf icons
				for (int i = 1; i < nextLevelUnlockCounter; i++) {
					unlockPanel.transform.GetChild(i).GetComponent<Image>().enabled = true;
				}
				StartCoroutine(FadeInLeaf (nextLevelUnlockCounter));
				// Unlock new level and reset leaves
				if (nextLevelUnlockCounter == numGamesToUnlockLevel) {
					nextLevelUnlockCounter = 0;
					SecurePlayerPrefs.SetBool(SavedData.UnlockedLevel[nextLevelToUnlock], true);
					SecurePlayerPrefs.SetInt(SavedData.InitScore, initScore + scorePerLevel);
					StartCoroutine(NewLevelUnlock(nextLevelToUnlock));

					GoogleAnalyticsV3.instance.LogEvent("Levels", "Unlock", nextLevelToUnlock.ToString(), 1);
				}
			}
			// Below unlock score, reset
			else {
				StartCoroutine(FadeOutLeaf(nextLevelUnlockCounter));
				nextLevelUnlockCounter = 0;
			}
		}
		// No more new levels
		else {
			unlockPanel.SetActive(false);
		}

		SecurePlayerPrefs.SetInt(SavedData.NextLevelUnlockCounter, nextLevelUnlockCounter);
	}

	public void ReportScoreToLeaderboards() {

		Social.ReportScore(score, LEADERBOARD_ID, (bool success) => {
		});
	}

	void ReportAchievements() {
		Achievements.IncrementProgress(Achievements.Leaves1000, numLeaves);
		Achievements.IncrementProgress(Achievements.Canes1000, numCanes);

		bool startedFromHighestLevel = SecurePlayerPrefs.GetInt(SavedData.InitScore) == highestInitScore;
		if (startedFromHighestLevel && numLeaves >= 10) {
			Achievements.Award(Achievements.OMNOMNOM);
		}
		if (startedFromHighestLevel && numCanes >= 10) {
			Achievements.Award(Achievements.CantTouchThis);
		}

		// Check if all levels have been unlocked
		bool unlockedAllLevels = true;
		for (int i = 0; i < SavedData.UnlockedLevel.Length; i++) {
			bool unlockedCurrLevel = SecurePlayerPrefs.GetBool(SavedData.UnlockedLevel[i]);
			if (!unlockedCurrLevel) {
				unlockedAllLevels = false;
				break;
			}
		}
		if (unlockedAllLevels)
			Achievements.Award(Achievements.GreediestGrubby);
	}


	void IncreaseBonus () {
		score += scorePerBonus;
	}

	public void IncreaseNumCanes () {
		numCanes++;
		IncreaseBonus();
	}

	public void IncreaseNumLeaves () {
		numLeaves++;
		IncreaseBonus();
	}

	public int GetScore() {
		return score;
	}
	
	IEnumerator FadeInLeaf(int i) {
		yield return new WaitForSeconds(0.75f);
		unlockPanel.transform.GetChild(i).GetComponent<Image>().enabled = true;
		unlockPanel.transform.GetChild(i).GetComponent<CanvasFader>().FadeIn();
	}
	
	IEnumerator FadeOutLeaf(int levelUnlockCount) {
		for (int i = levelUnlockCount; i > 0; i--) {
			unlockPanel.transform.GetChild(i).GetComponent<Image>().enabled = true;
		}
		yield return new WaitForSeconds(0.75f);
		for (int i = levelUnlockCount; i > 0; i--) {
			unlockPanel.transform.GetChild(i).GetComponent<CanvasFader>().FadeOut();
			yield return new WaitForSeconds(0.5f);
		}
	}
	
	IEnumerator NewLevelUnlock(int nextUnlock) {
		// Wait for panel to fully load
		yield return new WaitForSeconds(1.5f);
		// Fade out and wait
		unlockPanel.GetComponent<CanvasFader>().FadeOut();
		yield return new WaitForSeconds(unlockPanel.GetComponent<CanvasFader>().fadeTime);
		// Change unlock score and disable unlocked leaves
		unlockPanel.transform.GetChild(0).GetComponent<Text>().text = (SecurePlayerPrefs.GetInt(SavedData.InitScore) + scorePerLevel).ToString() + "+";
		for (int i = 1; i <= 3; i++) {
			unlockPanel.transform.GetChild(i).GetComponent<Image>().enabled = false;
		}
		// Fade in, faster than fade out
		if (nextUnlock < SavedData.UnlockedLevel.Length - 1) {
			unlockPanel.GetComponent<CanvasFader>().fadeTime = 0.15f;
			unlockPanel.GetComponent<CanvasFader>().FadeIn();
		}
	}
	
	
	void SetDifficulty() {
		if (isTutorial) return;
		
		if (score > 300) {
			newSpeed = -3.85f;
			if (Generator.instance.speed > newSpeed) {
				Generator.instance.ChangeDifficulty(newSpeed, 2f);
				grubbusScript.Enter();
				character.ChangePullSpeed(3f);
			}
		}
		else if (score > 250) {
			newSpeed = -3.75f;
			if (Generator.instance.speed > newSpeed) {
				Generator.instance.ChangeDifficulty(newSpeed, 2f);
				grubbusScript.Enter();
				character.ChangePullSpeed(3f);
			}
		}
		else if (score > 200) {
			newSpeed = -3.5f;
			if (Generator.instance.speed > newSpeed) {
				Generator.instance.ChangeDifficulty(newSpeed, 2.1f);
				grubbusScript.Enter();
				character.ChangePullSpeed(3f);
			}
		}
		else if (score > 150) {
			newSpeed = -3.25f;
			if (Generator.instance.speed > newSpeed) {
				Generator.instance.ChangeDifficulty(newSpeed, 2.2f);
				grubbusScript.Enter();
				character.ChangePullSpeed(2.75f);
			}
		}
		else if (score > 100) {
			newSpeed = -3f;
			if (Generator.instance.speed > newSpeed) {
				Generator.instance.ChangeDifficulty(newSpeed, 2.3f);
				grubbusScript.Enter();
				character.ChangePullSpeed(2.5f);
			}
		}
		else if (score > 50) {
			newSpeed = -2.75f;
			if (Generator.instance.speed > newSpeed) {
				Generator.instance.ChangeDifficulty(newSpeed, 2.4f);
				character.ChangePullSpeed(2.25f);
			}
		}
		else if (score > 0) {
			newSpeed = -2.5f;
			if (Generator.instance.speed > newSpeed) {
				Generator.instance.ChangeDifficulty(newSpeed, 2.5f);
				character.ChangePullSpeed(2f);
			}
		}
		
	}
}
