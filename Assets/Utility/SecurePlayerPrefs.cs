﻿using UnityEngine;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System;

public static class SecurePlayerPrefs
{
	static readonly string PASSWORD = "#Hpy5i9[3V`f2G+90s43:45*3sg?Gh";


	public static void SetDefaultInt(string key, int defaultValue) {
		if (!HasKey(key))
			SetInt(key, defaultValue);
	}

	public static void SetInt(string key, int value) {
		string stringValue = value.ToString();
		SetString(key, stringValue);
	}

	public static int GetInt(string key) {
		string stringValue = GetString(key);
		return Int32.Parse(stringValue);
	}


	public static void SetDefaultBool(string key, bool defaultValue) {
		if (!HasKey(key))
			SetBool(key, defaultValue);
	}
	
	public static void SetBool(string key, bool value) {
		string stringValue = value.ToString();
		SetString(key, stringValue);
	}
	
	public static bool GetBool (string key) {
		string stringValue = GetString(key);
		return bool.Parse(stringValue);
	}


	public static void SetDefaultFloat(string key, float defaultValue) {
		if (!HasKey(key))
			SetFloat(key, defaultValue);
	}
	
	public static void SetFloat(string key, float value) {
		string stringValue = value.ToString();
		SetString(key, stringValue);
	}
	
	public static float GetFloat(string key) {
		string stringValue = GetString(key);
		return float.Parse(stringValue);
	}


	public static void SetDefaultString(string key, string defaultValue) {
		if (!HasKey(key))
			SetString(key, defaultValue);
	}
	
	public static void SetString(string key, string value)
	{
		var desEncryption = new DESEncryption();
		string hashedKey = GenerateMD5(key);
		string encryptedValue = desEncryption.Encrypt(value, PASSWORD);
		PlayerPrefs.SetString(hashedKey, encryptedValue);
		PlayerPrefs.Save ();
	}
	
	public static string GetString(string key)
	{
		string hashedKey = GenerateMD5(key);
		if (PlayerPrefs.HasKey(hashedKey))
		{
			var desEncryption = new DESEncryption();
			string encryptedValue = PlayerPrefs.GetString(hashedKey);
			string decryptedValue;
			desEncryption.TryDecrypt(encryptedValue, PASSWORD, out decryptedValue);
			return decryptedValue;
		}
		else
		{
			return "";
		}
	}
	
	public static string GetString(string key, string defaultValue)
	{
		if (HasKey(key))
		{
			return GetString(key);
		}
		else
		{
			return defaultValue;
		}
	}
	
	public static bool HasKey(string key)
	{
		string hashedKey = GenerateMD5(key);
		bool hasKey = PlayerPrefs.HasKey(hashedKey);
		return hasKey;
	}
	
	/// <summary>
	/// Generates an MD5 hash of the given text.
	/// WARNING. Not safe for storing passwords
	/// </summary>
	/// <returns>MD5 Hashed string</returns>
	/// <param name="text">The text to hash</param>
	static string GenerateMD5(string text)
	{
		var md5 = MD5.Create();
		byte[] inputBytes = Encoding.UTF8.GetBytes(text);
		byte[] hash = md5.ComputeHash(inputBytes);
		
		// step 2, convert byte array to hex string
		var sb = new StringBuilder();
		for (int i = 0; i < hash.Length; i++)
		{
			sb.Append(hash[i].ToString("X2"));
		}
		return sb.ToString();
	}
}


public interface IEncryption
{
	string Encrypt(string plainText, string password);
	bool TryDecrypt(string cipherText, string password, out string plainText);
}

public class DESEncryption : IEncryption
{
	const int Iterations = 1;
	
	public string Encrypt(string plainText, string password)
	{
		if (plainText == null)
		{
			throw new ArgumentNullException("plainText");
		}
		
		if (string.IsNullOrEmpty(password))
		{
			throw new ArgumentNullException("password");
		}
		
		// create instance of the DES crypto provider
		var des = new DESCryptoServiceProvider();
		
		// generate a random IV will be used a salt value for generating key
		des.GenerateIV();
		
		// use derive bytes to generate a key from the password and IV
		var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, des.IV, Iterations);
		
		// generate a key from the password provided
		byte[] key = rfc2898DeriveBytes.GetBytes(8);
		
		// encrypt the plainText
		using (var memoryStream = new MemoryStream())
			using (var cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(key, des.IV), CryptoStreamMode.Write))
		{
			// write the salt first not encrypted
			memoryStream.Write(des.IV, 0, des.IV.Length);
			
			// convert the plain text string into a byte array
			byte[] bytes = Encoding.UTF8.GetBytes(plainText);
			
			// write the bytes into the crypto stream so that they are encrypted bytes
			cryptoStream.Write(bytes, 0, bytes.Length);
			cryptoStream.FlushFinalBlock();
			
			return Convert.ToBase64String(memoryStream.ToArray());
		}
	}
	
	public bool TryDecrypt(string cipherText, string password, out string plainText)
	{
		// its pointless trying to decrypt if the cipher text
		// or password has not been supplied
		if (string.IsNullOrEmpty(cipherText) || 
		    string.IsNullOrEmpty(password))
		{
			plainText = "";
			return false;
		}
		
		try
		{   
			byte[] cipherBytes = Convert.FromBase64String(cipherText);
			
			using (var memoryStream = new MemoryStream(cipherBytes))
			{
				// create instance of the DES crypto provider
				var des = new DESCryptoServiceProvider();
				
				// get the IV
				byte[] iv = new byte[8];
				memoryStream.Read(iv, 0, iv.Length);
				
				// use derive bytes to generate key from password and IV
				var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, iv, Iterations);
				
				byte[] key = rfc2898DeriveBytes.GetBytes(8);
				
				using (var cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(key, iv), CryptoStreamMode.Read))
					using (var streamReader = new StreamReader(cryptoStream))
				{
					plainText = streamReader.ReadToEnd();
					return true;
				}
			}
		}
		catch(Exception ex)
		{
			// TODO: log exception
			Console.WriteLine(ex);
			
			plainText = "";
			return false;
		}
	}
}