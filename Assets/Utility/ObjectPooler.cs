﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour {
	
	public static ObjectPooler instance;
	public GameObject pooledObject;
	public int pooledAmount = 20;
	public bool willGrow = true;
	
	List<GameObject> pooledObjects;
	
//	void Awake () {
//		DontDestroyOnLoad(transform.gameObject);
//		
//		// Only 1 instance of a pooler given a pooled object
//		ObjectPooler[] objectPoolers = FindObjectsOfType<ObjectPooler>();
//		foreach (ObjectPooler objectPooler in objectPoolers) {
//			if (!objectPooler.Equals(this) && pooledObject == objectPooler.pooledObject) {
//				DestroyImmediate(objectPooler.gameObject);
//			}
//		}
//	}
	
	// Use this for initialization
	void Start () {
		// Make pooledAmount of pooledObject
		if (pooledObjects == null) {
			pooledObjects = new List<GameObject>();
			for (int i=0; i<pooledAmount; i++) {
				GameObject obj = (GameObject) Instantiate (pooledObject);
				obj.SetActive(false);
				pooledObjects.Add (obj);
			}
		}
	}
	
	public GameObject GetPooledObject() {
		for (int i=0; i<pooledObjects.Count; i++) {
			if (!pooledObjects[i].activeInHierarchy) {
				return pooledObjects[i];
			}
		}
		if (willGrow){
			GameObject obj = (GameObject) Instantiate (pooledObject);
			pooledObjects.Add (obj);
			return obj;
		}
		return null;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
