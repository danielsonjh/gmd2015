﻿using UnityEngine;
using System.Collections;

using Soomla.Profile;
using Soomla.Store;

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class Initializer : MonoBehaviour {
	
	public static Initializer instance;

	void Awake() {
		// Only allow 1 instance
		if(instance)
			DestroyImmediate(gameObject);
		else {
			DontDestroyOnLoad(gameObject);
			instance = this;
		}
	}

	// Use this for initialization
	void Start () {
		Application.runInBackground = true;
		InitSoomla();
		InitGooglePlayServices();
		InitGoogleAnalytics();
		InitSavedData();
	}

	void InitSavedData() {
		SavedData.Initialize();

		AudioManager.instance.ToggleBGM(SecurePlayerPrefs.GetBool(SavedData.EnabledMusic));
		AudioManager.instance.ToggleSFX(SecurePlayerPrefs.GetBool(SavedData.EnabledSFX));
	}

	void InitSoomla() {
		SoomlaProfile.Initialize();
		SoomlaStore.Initialize(new StoreAssets());
		GetComponent<SoomlaEvents>().Initialize();
		SoomlaStore.RestoreTransactions();
	}

	void InitGooglePlayServices() {
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
			// enables saving game progress.
			.EnableSavedGames()
			.Build();
		
		PlayGamesPlatform.InitializeInstance(config);

		// DEBUG MODE:
		PlayGamesPlatform.DebugLogEnabled = false;
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();

		Social.localUser.Authenticate((bool success) => {
			;
		});
	}

	void InitGoogleAnalytics() {
		GoogleAnalyticsV3.instance.logLevel = GoogleAnalyticsV3.DebugMode.ERROR;
	}
}
