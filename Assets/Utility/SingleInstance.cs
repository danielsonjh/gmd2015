﻿using UnityEngine;
using System.Collections;

public class SingleInstance : MonoBehaviour {

	public bool isFirst = false;

	void Awake () {
		// Check all other SingleInstances to find pre-existing ones
		SingleInstance[] singleInstances = FindObjectsOfType<SingleInstance>();
		foreach (SingleInstance singleInstance in singleInstances) {
			if (AlreadyExists(singleInstance)) {
				DestroyImmediate(gameObject);
				return;
			}
		}

		// First instance: keep object between scenes, and assign the name
		isFirst = true;
		DontDestroyOnLoad(gameObject);
	}

	bool AlreadyExists(SingleInstance singleInstance) {
		return singleInstance.isFirst && singleInstance.gameObject.name == gameObject.name;
	}
}
