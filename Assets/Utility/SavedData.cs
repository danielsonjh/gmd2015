﻿using UnityEngine;
using System.Collections;

using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Soomla.Store;

public static class SavedData {

	public static readonly string HighScore = "HighScore";
	public static readonly string InitScore = "InitScore";
	public static readonly string SelectedLevel = "SelectedLevel";
	public static readonly string NextLevelUnlockCounter = "NextLevelUnlockCounter";
	public static readonly string[] UnlockedLevel = {"UnlockedLevel0", "UnlockedLevel1", "UnlockedLevel2", "UnlockedLevel3", "UnlockedLevel4"};
	
	public static readonly string EnabledMusic = "EnabledMusic";
	public static readonly string EnabledSFX = "EnabledSFX";
	public static readonly string EnabledTutorial = "EnabledTutorial";
	
	public static readonly string UnlockedEnlightened = "UnlockedEnlightened";
	public static readonly string EnabledEnlightened = "EnabledEnlightened";
	public static readonly string EnabledAds = "EnabledAds";

	public static void Initialize() {
		SecurePlayerPrefs.SetDefaultInt(HighScore, -1);
		SecurePlayerPrefs.SetDefaultInt(InitScore, 0);
		SecurePlayerPrefs.SetDefaultInt(SelectedLevel, 0);
		SecurePlayerPrefs.SetDefaultInt(NextLevelUnlockCounter, 0);

		SecurePlayerPrefs.SetDefaultBool(UnlockedLevel[0], true);
		for (int i = 1; i < UnlockedLevel.Length; i++) {
			SecurePlayerPrefs.SetDefaultBool(UnlockedLevel[i], false);
		}

		SecurePlayerPrefs.SetDefaultBool(EnabledMusic, true);
		SecurePlayerPrefs.SetDefaultBool(EnabledSFX, true);
		SecurePlayerPrefs.SetDefaultBool(EnabledTutorial, true);

		bool playerDonated = StoreInventory.GetItemBalance(StoreAssets.DONATE1.ItemId) > 0;
		SecurePlayerPrefs.SetDefaultBool(UnlockedEnlightened, playerDonated);
		SecurePlayerPrefs.SetDefaultBool(EnabledEnlightened, false);
		SecurePlayerPrefs.SetDefaultBool(EnabledAds, false);
	}
}
