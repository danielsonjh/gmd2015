﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ExtensionMethods
{
	public static class MyExtensions
	{
		public static void Shuffle<T>(this List<T> list)
		{
			int n = list.Count;  
			while (n > 1) {  
				n--;
				int k = UnityEngine.Random.Range(0, n+1);  
				T value = list[k];  
				list[k] = list[n];
				list[n] = value;  
			}
		}

		public static string FirstCharToUpper(this string str)
		{
			if (str == null)
				return null;
			
			if (str.Length > 1)
				return char.ToUpper(str[0]) + str.Substring(1);
			
			return str.ToUpper();
		}
	}   
}