﻿using UnityEngine;
using System.Collections;

using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class Achievements : MonoBehaviour {

	public static readonly string GreediestGrubby = "CgkIg9u6lZMTEAIQAQ";
	public static readonly string EnlightenedGrubby = "CgkIg9u6lZMTEAIQAw";
	public static readonly string SharingIsCaring = "CgkIg9u6lZMTEAIQBA";
	public static readonly string OMNOMNOM = "CgkIg9u6lZMTEAIQBQ";
	public static readonly string CantTouchThis = "CgkIg9u6lZMTEAIQBg";
	public static readonly string Leaves1000 = "CgkIg9u6lZMTEAIQBw";
	public static readonly string Canes1000 = "CgkIg9u6lZMTEAIQCA";

	public static void Award(string achievementID) {
		Social.ReportProgress(achievementID, 100.0, (bool success) => {
			// handle success or failure
		});
	}

	public static void IncrementProgress(string achievementID, int incrementValue) {
		PlayGamesPlatform.Instance.IncrementAchievement(
			achievementID, incrementValue, (bool success) => {
			// handle success or failure
		});
	}

	//public static void SetProgress(string achievementID, int setValue) {};
}
