﻿using UnityEngine;
using System.Collections;

public class SilkAnimator : MonoBehaviour {

	
	public float initWidth = 4;
	public float maxWidth = 6;
	public float minWidth = 2;
	public float frequency = 10;
	
	
	
	private float speed;
	private bool growing = true;
	
	private float maxDestroyWidth = 20;
	public float destroyTime = 0.2f;
	private bool destroying = false;

	void Start () {
		if (maxWidth < minWidth) Debug.Break();
		speed = frequency * (maxWidth - minWidth);
	}

	void Update () {
		if (destroying) {
			DestroyAnimation ();
			return;	
		}
	
		if (growing) {
			if (transform.localScale.x < maxWidth) {
				transform.localScale = new Vector2(transform.localScale.x + speed * Time.deltaTime, transform.localScale.y);
			} else {
				growing = false;
			}
		}
		else {
			if (transform.localScale.x > minWidth)
				transform.localScale = new Vector2(transform.localScale.x - speed * Time.deltaTime, transform.localScale.y);
			else
				growing = true;
		}
	}
	
	void DestroyAnimation() {
		transform.localScale = new Vector2(transform.localScale.x + speed * Time.deltaTime, transform.localScale.y);
		float alpha = GetComponent<SpriteRenderer>().color.a;
		GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, alpha - Time.deltaTime / destroyTime);
		
		if (transform.localScale.x > maxDestroyWidth) {
			Destroy(gameObject);
		}
		
	}
	
	public void FlagDestroyAnimation() {
		destroying = true;
		speed = (maxDestroyWidth - transform.localScale.x) / destroyTime;
	}
}
