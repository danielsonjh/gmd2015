﻿using UnityEngine;
using System.Collections;

public class CharacterAnimator : MonoBehaviour {

	public AnimationClip e_Shoot_E;
	public Sprite[] e_seg;

	private Character characterScript;
	private Animator anim;
	private bool mouseWasUp;
	private bool isShooting = true;

	
	private SpriteRenderer eyes;
	private SpriteRenderer seg1;
	private SpriteRenderer seg2;
	private SpriteRenderer seg3;
	
	private float angle;
	
	// Use this for initialization
	void Start () {
//		Time.timeScale = 0.5f;
		characterScript = GameObject.Find ("Character").GetComponent<Character>();
		anim = GetComponent<Animator>();
		
		eyes = GameObject.Find ("Eyes").GetComponent<SpriteRenderer>();
		seg1 = GameObject.Find ("Seg1").GetComponent<SpriteRenderer>();
		seg2 = GameObject.Find ("Seg2").GetComponent<SpriteRenderer>();
		seg3 = GameObject.Find ("Seg3").GetComponent<SpriteRenderer>();

		if (SecurePlayerPrefs.GetBool(SavedData.EnabledEnlightened)) {
			SetCurrentAnimation("Shoot_E", e_Shoot_E);
			seg1.sprite = e_seg[0];
			seg2.sprite = e_seg[1];
			seg3.sprite = e_seg[2];
			anim.Rebind();
		}
		else {
			transform.FindChild("EnlightenedFroth").gameObject.SetActive(false);
			transform.FindChild("EnlightenedParticles").gameObject.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {		
		// Check if shooting animation is done
		if (isShooting && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.1f) {
			isShooting = false;
		}
		// Set silk hit parameter (allow transition between adjacent states) only if shooting animation is done
		if (!isShooting) {
			anim.SetBool ("hit", characterScript.IsSilkHit());
		}

		// Set the animator parameter "angle"
		Vector2 deltaPos = characterScript.hitPos - (Vector2)transform.position;
		angle = Mathf.Atan (deltaPos.y / deltaPos.x) * 180f / Mathf.PI;
		if (deltaPos.x < 0) angle *= -1; // Flip the sign of the angle if it is on the left half plane
		anim.SetFloat("angle", angle);
		
		// Only trigger shooting animation when silk hasn't been cut
		if (mouseWasUp && !characterScript.silkCut) {
			ShootingAnimation();
		}
		
		// Flip layer order according to the sign of deltaPos.y
		if (deltaPos.y > 0) {
			eyes.sortingOrder = 4;
			seg1.sortingOrder = 5;
			seg2.sortingOrder = 6;
			seg3.sortingOrder = 7;
		}
		else {
			eyes.sortingOrder = 7;
			seg1.sortingOrder = 6;
			seg2.sortingOrder = 5;
			seg3.sortingOrder = 4;
		}
		
		// Flip the world according to the sign of deltaPos.x
		Vector3 theScale = transform.localScale;
		theScale.x = Mathf.Sign (deltaPos.x);
		transform.localScale = theScale;

		// Remember if mouse was up last frame (to prevent old angle values from setting the animation state)
		mouseWasUp = Input.GetMouseButtonUp(0);
	}
	
	bool IsBetween(float n, float lower, float upper) {
		return lower < n && n < upper;
	}

	
	void ShootingAnimation() {
		isShooting = true;
		anim.SetBool ("hit", false);
		anim.SetTrigger ("trigger");
	}
	
	public void SimulateClick(Vector2 simMousePos) {
		// Set the animator parameter "angle"
		Vector2 deltaPos = simMousePos - (Vector2)transform.position;
		angle = Mathf.Atan (deltaPos.y / deltaPos.x) * 180f / Mathf.PI;
		if (deltaPos.x < 0) angle *= -1; // Flip the sign of the angle if it is on the left half plane
		anim.SetFloat("angle", angle);

		StartCoroutine(Wait());
	}

	IEnumerator Wait() {
		yield return new WaitForEndOfFrame();
		anim.SetTrigger ("trigger");
	}

	void SetCurrentAnimation(string clipName, AnimationClip animClip)
	{
		RuntimeAnimatorController myController = anim.runtimeAnimatorController;
		AnimatorOverrideController animatorOverride = new AnimatorOverrideController();
		animatorOverride.runtimeAnimatorController = myController;
		
		animatorOverride[clipName] = animClip;
		anim.runtimeAnimatorController = animatorOverride;
	}
}
