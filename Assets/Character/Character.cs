﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

	public GameObject mouseTapObject;
	public GameObject silkGameObject;
	private Rigidbody2D leaf;
	private float pullSpeed = 3f;
	
	private bool swipeEnabled = true;
	private bool silkEnabled = true;
	private bool notPaused;
	public bool silkCut;
	private bool mouseReleased;
	private Vector2 mouseDownPos;
	private Vector2 mouseUpPos;
	private Vector2 mouseDeltaPos;
	private Vector2 mouseCurrPos;
	private Vector2 mousePrevPos;
	private float clickThreshold = 1f;
	
	public Vector2 hitPos;
	public bool hasSilk = true;
	private GameObject silkClone;
	private Silk silkObject;
	
	private GameObject mouseTrail;
	private Vector2 mousePos;
	private float deltaY;
	private float deltaX;
	private float angle;
	private float prevSignX;
	private float signSum;


	// Use this for initialization
	void Start () {
		hitPos = new Vector2 (3.5f, 0f); // For start orientation
		leaf = GetComponentInParent<Rigidbody2D>();
		mouseTrail = GameObject.Find ("MouseTrail");

		AudioManager.instance.PlayClip(AudioManager.instance.splash);
	}
	
	// Update is called once per frame
	void Update () {
		mousePos.y += Generator.instance.speed * Time.deltaTime;
	
		signSum = 0;
		mouseReleased = false;
		
		// Mouse pressed
		if (Input.GetMouseButtonDown(0)) {
			mouseDownPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mousePrevPos = mouseDownPos;
			mouseDeltaPos = Vector2.zero;
			// New click, so silk has not been cut yet
			silkCut = false;
			// Enable silk if NOT Paused yet
			notPaused = Time.timeScale > 0;
		}
		
		// Mouse dragged
		if (Input.GetMouseButton (0) && swipeEnabled) {
			// Get change in mouse position
			mouseCurrPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mouseDeltaPos = mouseCurrPos - mousePrevPos;
			
			// Set mouse trail
			mouseTrail.transform.position = mouseCurrPos;
			
			// If the mouse moved between frames
			if (mouseDeltaPos != Vector2.zero) {
				//Debug.DrawRay (mousePrevPos, mouseDeltaPos, Color.red, 0.5f);
				RaycastHit2D[] hits = Physics2D.RaycastAll(mousePrevPos, mouseDeltaPos, mouseDeltaPos.magnitude);
				foreach (RaycastHit2D hit in hits) {
					// If the mouse crossed a swipe collider, destroy the silk
					if (hit.collider != null && hit.collider.name == "Swipe Collider") {					
						hit.transform.GetComponentInParent<Silk>().DestroySilk();
						silkCut = true;
					}
				}
			}
			
			// Reset prev position
			mousePrevPos = mouseCurrPos;
		}
		
		// Mouse released
		if (Input.GetMouseButtonUp(0)) {
			mouseReleased = true;
			mouseUpPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mousePrevPos = mouseUpPos;
			mouseDeltaPos = mouseUpPos - mouseDownPos;
			// Enabled silk if STILL NOT paused yet (timeScale state changes between UP and DOWN if pausing/unpausing)
			// This means the game was not already paused, and this click did not cause the game to pause
			notPaused = notPaused && Time.timeScale > 0;
		}

		
		hasSilk = silkClone == null;

		// If silk is unused, mouse is released, and silk was not cut with the current click, shoot one
		if (mouseReleased && !silkCut && mouseDeltaPos.magnitude < clickThreshold && notPaused && silkEnabled) {
			if (!hasSilk) {
				silkClone.GetComponent<Silk>().DestroySilk();
			}
			
			mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			ShootSilk ();
		}
		
		// If silk is being used
		if (!hasSilk) {
			
			// Get the correct hitPos (based on if silk hit object or not)
			if (silkObject.hitObjectColl != null) {
				hitPos = silkObject.hitObjectColl.transform.position;
				// Only scale the silk if it hit something
				silkClone.transform.localScale = new Vector3(1, Vector2.Distance(hitPos, transform.position), 1);
			}
			else {
				hitPos = mousePos;
				// If silk reaches click location, stop moving it down
				if ((mousePos - (Vector2)transform.position).magnitude < silkObject.transform.localScale.y) {
					mousePos.y -= Generator.instance.speed * Time.deltaTime;
				}
			}

			// Recalculate and reset the angle
			deltaX = hitPos.x - transform.position.x;
			angle = GetAngle (transform.position, hitPos);
			silkClone.transform.rotation = Quaternion.Euler (0, 0, angle);	
			
			// Adjust the silk position and size
			silkClone.transform.position = transform.position;
			
			// If silk hit an object
			if (silkObject.hitObjectColl != null) {
				// Stop and destroy the silk if the deltaX changes sign (velocity overshoot)
				if (prevSignX != Mathf.Sign (deltaX)) {
					leaf.velocity = Vector2.zero;
					silkObject.DestroySilk ();
					hasSilk = true;
				}
				// Remember the sign of deltaX
				prevSignX = Mathf.Sign(deltaX);
				// Do this because Mathf.Sign() returns 1 for zero input
				if (deltaX == 0) prevSignX = 0;
				// Add up all the signs for this frame
				signSum += prevSignX;
			}
		}
		
		// Move the leaf
		leaf.velocity = new Vector2(pullSpeed * signSum, 0);
	}
	
	
	float GetAngle(Vector2 start, Vector2 end) {
		float deltaY = end.y - start.y;
		float deltaX = end.x - start.x;
		float angle = Mathf.Atan(deltaY / deltaX) * 180 / Mathf.PI - 90; // Subtract 90 because of the way Quaternion works
		if (deltaX < 0) angle += 180;	// Flip angles over the Y axis, since +Y axis = 0, +X axis = -90, -Y = -180
		else if (deltaX == 0 && deltaY >= 0) angle = 0;
		else if (deltaX == 0 && deltaY < 0) angle = -180;
		
		return angle;
	}
	
	
	public void ShootSilk() {
		AudioManager.instance.PlayClip(AudioManager.instance.silkShoot);

		hasSilk = false;

		// Shoot a silk web
		silkClone = (GameObject) Instantiate (silkGameObject);

		// Get the Silk script
		silkObject = silkClone.GetComponent<Silk>();

		// Calculate and set the initial angle of the silk
		deltaX = mousePos.x - transform.position.x;
		angle = GetAngle(transform.position, mousePos);
		silkClone.transform.rotation = Quaternion.Euler (0, 0, angle);
		
		// Move and Scale the silk to initial values
		silkClone.transform.position = transform.position;
		silkClone.transform.localScale = new Vector3(1, 1, 1);
		
		// Remember the sign of deltaX so that a sign change (velocity overshoot) can be detected later
		prevSignX = Mathf.Sign(deltaX);
		
		// Visual feedback for click
		GameObject mouseTapClone = (GameObject) Instantiate (mouseTapObject);
		mouseTapClone.GetComponent<Animator>().speed = 3f;
		mouseTapClone.GetComponent<Animator>().SetTrigger("click");
		mouseTapClone.transform.position = mousePos;
	}
	
	
	public void SetMousePos(Vector2 v) {
		mousePos = v;
	}
	
	
	public void DestroySilk() {
		if (silkObject != null) {
			silkObject.DestroySilk();
		}
	}
	
	
	public bool IsSilkHit() {
		return silkObject!= null && silkObject.hitObjectColl != null;
	}
	
	
	public void DisableSilk() {
		silkEnabled = false;
	}
	
	public void EnableSilk() {
		silkEnabled = true;
	}
	
	public void DisableSwipe() {
		swipeEnabled = false;
	}
	
	public void EnableSwipe() {
		swipeEnabled = true;
	}

	public void ChangePullSpeed(float newPullSpeed) {
		pullSpeed = newPullSpeed;
		Debug.Log (pullSpeed);
	}
}
