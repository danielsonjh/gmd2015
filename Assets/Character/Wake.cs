﻿using UnityEngine;
using System.Collections;

public class Wake : MonoBehaviour {

	float lifeTime = .5f;
	float currLifeTime;
	SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		Init ();
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, Generator.instance.speed);
	
		currLifeTime -= Time.deltaTime;
		spriteRenderer.color = new Color(1, 1, 1, currLifeTime / lifeTime);
		float s = 1 + lifeTime - currLifeTime;
		transform.localScale = new Vector3(s, s, 1);
		if (currLifeTime < 0)
			gameObject.SetActive(false);
	}
	
	public void Init() {
		currLifeTime = lifeTime;
	}
}
