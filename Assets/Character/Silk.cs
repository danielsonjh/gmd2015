﻿using UnityEngine;
using System.Collections;

public class Silk : MonoBehaviour {

	public GameObject bonusTextPrefab;
	public Collider2D hitObjectColl;
	public Vector2 hitPos;
	public bool hitBonus;
	
	private Score scoreScript;
		
	private float speed = 10;
	private float maxSize = 8;

	// Use this for initialization
	void Start () {
		scoreScript = FindObjectOfType<Score>();
	}
	
	// Update is called once per frame
	void Update () {
		// Stretch the silk until it hits something
		if (hitObjectColl == null && transform.localScale.y < maxSize) {
			transform.localScale = new Vector3(1, transform.localScale.y + speed * Time.deltaTime, 1);
		}
		// Destroy the silk if hasn't hit anything AND it exceeds its range
		if (hitObjectColl == null && transform.localScale.y >= maxSize) {
			DestroySilk();
		}
	}
	
	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.name == "Rock(Clone)" || coll.name == "Rock") {
			hitObjectColl = coll;
			hitPos = coll.transform.position;
			
			// Check for bonus
			Rock rockScript = coll.gameObject.GetComponent<Rock>();
			if (rockScript.hasBonus) {
				AudioManager.instance.PlayClip(AudioManager.instance.bonus);
				// Increase bonus and remove sprite
				rockScript.hasBonus = false;
				scoreScript.IncreaseNumLeaves();
				ShowBonusText();
			}
		}
		
		else if ((coll.name == "Cane(Clone)" || coll.name == "Cane") && hitObjectColl == null) {
			hitPos = coll.transform.position;
			// Destroying canes increases bonus
			coll.gameObject.SetActive(false);
			scoreScript.IncreaseNumCanes();
			ShowBonusText();
			DestroySilk ();
		}
	}

	void ShowBonusText() {
		bonusTextPrefab = Instantiate(bonusTextPrefab);
		bonusTextPrefab.GetComponent<BonusText>().Show(hitPos);
	}
	
	public void DestroySilk() {
		AudioManager.instance.PlayClip(AudioManager.instance.silkCut);

		SilkAnimator[] silkAnimators = GetComponentsInChildren<SilkAnimator>();
		foreach (SilkAnimator silkAnimator in silkAnimators) {
			silkAnimator.FlagDestroyAnimation();
		}
		
		if (transform.childCount >= 1) {
			Destroy (transform.GetChild(0).gameObject); // Destroy the silk swipe collider first;
			transform.DetachChildren();
		}
		Destroy(transform.gameObject);
	}
}
