﻿using UnityEngine;
using System.Collections;

public class Boat : MonoBehaviour {

	public bool isInvincible = false;

	float wakeTimerInit = 0.2f;
	float wakeTimer;

	private ObjectPooler wakePool;
	private Menu menuScript;

	// Use this for initialization
	void Start () {
		wakePool = GetComponent<ObjectPooler>();
		wakeTimer = wakeTimerInit;
		
		menuScript = FindObjectOfType<Menu>();
		
	}
	
	void Update() {
		wakeTimer -= Time.deltaTime;
	
		if (wakeTimer < 0) {
			wakeTimer = wakeTimerInit;
			GameObject wakeClone = wakePool.GetPooledObject();
			wakeClone.transform.position = transform.position + Vector3.down * 0.2f;
			wakeClone.GetComponent<Wake>().Init();
			wakeClone.SetActive(true);
		}
	}
	
	
	void OnTriggerEnter2D(Collider2D coll) {
		if (isInvincible) return;
	
		// Hit a rock, so die
		if (coll.name == "Rock(Clone)" || coll.name == "Rock" || coll.name == "Cane(Clone)" || coll.name == "Cane") {
			AudioManager.instance.PlayClip(AudioManager.instance.death);
			Camera.main.GetComponent<ScreenShaker>().Shake();
			GetComponentInChildren<Character>().DestroySilk();
			if (Application.loadedLevelName != "Tutorial")
				Invoke ("EnableDeathOverlay", 0.5f);
			else
				Invoke ("TutorialDeathOverlay", 0.5f);
			
			GameObject blood = GameObject.Find ("Blood");
			blood.transform.position = transform.position + Vector3.back * 5;
			blood.GetComponent<ParticleSystem>().Play();
			blood.GetComponent<Rigidbody2D>().velocity = new Vector2(0, Generator.instance.speed);
			
			gameObject.SetActive(false);
		}
	}
	
	void EnableDeathOverlay() {
		menuScript.EnableDeathOverlay();
	}
	
	void TutorialDeathOverlay() {
		FindObjectOfType<Tutor>().EnableDeathOverlay();
	}
}
