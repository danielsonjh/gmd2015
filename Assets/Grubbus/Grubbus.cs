﻿using UnityEngine;
using System.Collections;

public class Grubbus : MonoBehaviour {

	private bool isEnabled = false;

	private Animator anim;
	private bool setAnimation;
	
	private GameObject boatObj;

	private float speed = 1.5f;
	private float caneSpeed = 4f;
	private float throwCooldown = 2.5f;
	private float nextThrowTime;
	
	private ObjectPooler canePool;

	float finalPosY;

	// Use this for initialization
	void Start () {
		anim = GetComponentInChildren<Animator>();
	
		canePool = GameObject.Find ("CanePool").GetComponent<ObjectPooler>();
		boatObj = GameObject.Find ("Boat");
		
		nextThrowTime = throwCooldown;
	}
	
	// Update is called once per frame
	void Update () {
		if (isEnabled) {
			if (transform.position.x < 1f) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Abs (GetComponent<Rigidbody2D>().velocity.x), 0);
			}
			else if (transform.position.x > 6f) {
				GetComponent<Rigidbody2D>().velocity = new Vector2(-Mathf.Abs (GetComponent<Rigidbody2D>().velocity.x), 0);
			}
		
			if (Generator.instance.moving) {
				nextThrowTime -= Time.deltaTime;
				if (nextThrowTime < 0.5f && !setAnimation) {
					setAnimation = true;
					anim.SetTrigger("throw");
				}
				if (nextThrowTime < 0) {
					ThrowCane ();
					setAnimation = false;
					nextThrowTime = throwCooldown;
				}
			}
		}
		else if (transform.position.y <= finalPosY) {
			isEnabled = true;
			GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
		}
	}
	
	
	void ThrowCane() {
		AudioManager.instance.PlayClip(AudioManager.instance.cane);
	
		Vector2 vectorFromBoat = (Vector2)transform.position -
			new Vector2(boatObj.transform.position.x + Random.Range(0, 0.3f),
			boatObj.transform.position.y);

	
		GameObject clone = canePool.GetPooledObject();
		clone.SetActive(true);
		clone.transform.position = new Vector2(transform.position.x + 0.3f, transform.position.y - 0.5f);
		
		float travelTime = vectorFromBoat.y / caneSpeed;
		float xVel = (-vectorFromBoat.x + Random.Range(-.25f, .25f)) / travelTime; // Need direction TO boat
		clone.GetComponent<Rigidbody2D>().velocity = new Vector2(xVel, -caneSpeed);
		
//		clone.rigidbody2D.velocity = -vectorFromBoat.normalized * caneSpeed;
	}

	public void SetPosition(float yPos) {
		// Move Grubbus offscreen slightly above the final Y position
		Vector2 pos = transform.position;
		transform.position = new Vector2(pos.x, yPos + 2.25f);
		finalPosY = yPos;
	}

	
	public void Enter() {
		if (transform.position.y > finalPosY)
			GetComponent<Rigidbody2D>().velocity = new Vector2(0, -0.75f);
	}
	

	public void ChangeThrowCooldown(float newCooldown) {
		throwCooldown = newCooldown;
	}
}
