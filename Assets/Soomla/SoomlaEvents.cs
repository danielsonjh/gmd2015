﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Soomla.Store;
using Soomla.Profile;
using ExtensionMethods;


public class SoomlaEvents : MonoBehaviour {

	public void Initialize() {
		StoreEvents.OnMarketPurchase += onMarketPurchase;

		ProfileEvents.OnLoginFailed += onLoginFailed;
		ProfileEvents.OnSocialActionFinished += onSocialActionFinished;
		ProfileEvents.OnSocialActionFailed += onSocialActionFailed;
	}

	public void onMarketPurchase (PurchasableVirtualItem pvi, string payload, Dictionary<string, string> extra)
	{
		SecurePlayerPrefs.SetBool(SavedData.UnlockedEnlightened, true);
		SecurePlayerPrefs.SetBool(SavedData.EnabledEnlightened, true);
		Achievements.Award(Achievements.EnlightenedGrubby);	
		DialogBox.instance.ShowNotification("Grubby became enlightened! The effect can be toggled in Settings.");

		GoogleAnalyticsV3.instance.LogEvent("Conversion", "Donate", "Purchased", 1);
		GoogleAnalyticsV3.instance.LogTransaction(StoreAssets.DONATE1.Name, "", 1.0, 0, 0, "CAD");
	}

	public void onLoginFailed(Provider provider, String errorMessage, String userPayload) {
		DialogBox.instance.ShowNotification(provider.ToString().FirstCharToUpper() + " login failed.");
	}

	public void onSocialActionFinished(Provider provider, SocialActionType action, string payload) {
		// provider is the social provider
		// action is the social action (like, post status, etc..) that finished
		// payload is an identification string that you can give when you initiate the social action operation and want to receive back upon its completion

		if (action == SocialActionType.UPLOAD_IMAGE) {
			DialogBox.instance.ShowNotification(provider.ToString().FirstCharToUpper() + " share successful!");

			Achievements.Award(Achievements.SharingIsCaring);
			GoogleAnalyticsV3.instance.LogEvent("Social", "Upload Screenshot", provider.ToString(), 1);
			GoogleAnalyticsV3.instance.LogSocial(provider.ToString().FirstCharToUpper(), "Upload Screenshot", "");
		}
	}

	public void onSocialActionFailed(Provider provider, SocialActionType action, string message, string payload) {
		// provider is the social provider
		// action is the social action (like, post status, etc..) that failed
		// message is the failure message
		// payload is an identification string that you can give when you initiate the social action operation and want to receive back upon failure
		
		if (action == SocialActionType.UPLOAD_IMAGE) {
			DialogBox.instance.ShowNotification("Share failed!");
		}
	}
}

