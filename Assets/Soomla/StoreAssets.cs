﻿using UnityEngine;
using System.Collections;
using Soomla.Store;

public class StoreAssets : IStoreAssets {

	public int GetVersion() {
		return 0;
	}

	public VirtualCurrency[] GetCurrencies() {
		return new VirtualCurrency[] {};
	}
	
	public VirtualGood[] GetGoods() {
		return new VirtualGood[] {DONATE1, TEST_PURCHASED};
	}
	
	public VirtualCurrencyPack[] GetCurrencyPacks() {
		return new VirtualCurrencyPack[] {};
	}
	
	public VirtualCategory[] GetCategories() {
		return new VirtualCategory[] {};
	}

	/** Virtual Goods **/
	// Name
	// Description
	// Item ID
	public static VirtualGood DONATE1 = new SingleUseVG(
		"Donate 1",
		"Donate 1",
		"donate1",
		new PurchaseWithMarket("donate1", 1.00)
		);

	public static VirtualGood TEST_PURCHASED = new SingleUseVG(
		"Test",
		"Test",
		"android.test.purchased",
		new PurchaseWithMarket("android.test.purchased", 1.00)
		);

}
