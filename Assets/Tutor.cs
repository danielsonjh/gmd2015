﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Tutor : MonoBehaviour {

	float tutorTimeScale = 0.8f;
	float videoTimeScale = 0.7f;

	private float elapsedTime = 0;
	private int currStep = 0;
	private int videoStep = 0;
	private bool mouseWasUp = false;
	private bool mouseWasDown = false;
	private bool skipVideo = false;
	
	float tapCooldownInit = .3f;
	float tapCooldown;
	
	private GameObject boatObject;
	private Score scoreScript;
	private Character characterScript;
	private CharacterAnimator characterAnimatorScript;
	private GameObject mouseTrail;

//	private GameObject mouseTapHelper;
	GameObject pauseButton;
	GameObject deathOverlay;
	GameObject guideText;
	Text helpText;
	GameObject slider;
	
	public bool clickedHelper;

	// Categories
	string tutorialTag = "Tutorial";
	// Actions
	string stageTag = "Stage";
	string deathTag = "Death";

	// Use this for initialization
	void Start () {	
		scoreScript = FindObjectOfType<Score>();
		boatObject = GameObject.Find ("Boat");
		characterScript = boatObject.GetComponentInChildren<Character>();
		characterAnimatorScript = boatObject.GetComponentInChildren<CharacterAnimator>();
		characterScript.DisableSilk();
		characterScript.DisableSwipe();
		mouseTrail = GameObject.Find ("MouseTrail");
		mouseTrail.GetComponent<TrailRenderer>().enabled = false;

		pauseButton = GameObject.Find("Pause");
		// Remove death overlay
		deathOverlay = gameObject.transform.GetChild(2).gameObject;
		deathOverlay.SetActive (false);
		
		// Remove help
		guideText = GameObject.Find ("GuideText");
		helpText = guideText.GetComponentInChildren<Text>();
		helpText.text = "";
		
		// Set up slider
		slider = GameObject.Find ("Slider");
		slider.GetComponent<Slider>().interactable = false;
		ChangeTimescale(videoTimeScale);
		
		StartMoving ();

		GoogleAnalyticsV3.instance.LogEvent(tutorialTag, stageTag, "1-Intro", 1);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonUp(0)) {
			mouseWasUp = true;
		}
		if (Input.GetMouseButtonDown(0)) {
			mouseWasDown = true;
		}
	
		if (!boatObject.activeInHierarchy) {
			StopMoving();
		}

		switch (currStep) {
			case 0:
				StopMoving ();
				helpText.text = "Watch the DEMO to see the many different ways Grubby can use silk.\nMaster them all to pass the TUTORIAL.\n\n\n";
				
				break;
			case 1:
				// Disable text
				if (guideText.GetComponent<CanvasGroup>().alpha == 1)
					guideText.GetComponent<CanvasFader>().FadeOut();
				
				// Enable slider
				if (!skipVideo) {
					slider.GetComponent<Slider>().interactable = true;
					if (slider.GetComponent<CanvasGroup>().alpha == 0)
						slider.GetComponent<CanvasFader>().FadeIn();
				}
				
				PlayVideo();
				mouseWasDown = false;	// Remove flag to detect full tap in next step (because the user may lift the tap on the slider after this step and accidentally skip the next one)
				tapCooldown = tapCooldownInit;	//Start cooldown to prevent user from accidentally clicking ahead
				break;
			case 2:
				boatObject.GetComponent<Boat>().isInvincible = false;
				// Cooldown to prevent user from accidentally clicking ahead
				tapCooldown -= Time.deltaTime;
				if (tapCooldown < 0) tapCooldown = 0;
				// Disable slider
				slider.GetComponent<Slider>().interactable = false;
				if (slider.GetComponent<CanvasGroup>().alpha == 1)
					slider.GetComponent<CanvasFader>().FadeOut();
				
				StopMoving();
				// Start counting time again
				ChangeTimescale(tutorTimeScale);
				elapsedTime = 0;
				
				// Enable text
				helpText.text = "TAP to SHOOT silk.\nSWIPE to CUT silk.\nDODGE rocks and canes.\nONE silk can be shot at any time.\n\nTAP to start tutorial.";
				if (guideText.GetComponent<CanvasGroup>().alpha == 0)
					guideText.GetComponent<CanvasFader>().FadeIn();
				if (mouseWasDown && mouseWasUp) {
					mouseWasDown = false;
					mouseWasUp = false;
					if (tapCooldown <= 0) {
						StartMoving();
						currStep++;
						
						boatObject.GetComponentInChildren<Character>().ChangePullSpeed(2f);

						GoogleAnalyticsV3.instance.LogEvent(tutorialTag, stageTag, "3-Start", 1);
					}
				}
			    break;
			case 3:
				// Count elapsed time to know when to end
				ChangeTimescale(tutorTimeScale);
				elapsedTime += Time.deltaTime;				
				// Disable text
				if (guideText.GetComponent<CanvasGroup>().alpha == 1)
					guideText.GetComponent<CanvasFader>().FadeOut();
				// Re-enable control
				if (mouseWasDown) {
					mouseWasDown = false;
					characterScript.EnableSilk();
					characterScript.EnableSwipe();
				}
				// End after a certain time ONLY if boat is still active
				if (elapsedTime > 15f && boatObject.activeInHierarchy) {
					mouseWasUp = false; // Clear flag to refresh tap detection for next step
					currStep++;
					tapCooldown = tapCooldownInit;	//Start cooldown to prevent user from accidentally clicking ahead
				}
				break;
			case 4:
				// Cooldown to prevent user from accidentally clicking ahead
				tapCooldown -= Time.deltaTime;
				if (tapCooldown < 0) tapCooldown = 0;
				// Enable text
				helpText.text = "Now you're ready for the real game!\nThe game will get more difficult as your score increases. As you get better, the game will automatically start you off at higher difficulties.\n\nTAP to start game.";
				if (guideText.GetComponent<CanvasGroup>().alpha == 0)
					guideText.GetComponent<CanvasFader>().FadeIn();
				if (mouseWasDown) {
					mouseWasDown = false;
					characterScript.DisableSilk();
					characterScript.DisableSwipe();
				}
				if (mouseWasUp) {
					mouseWasUp = false;
					if (tapCooldown <= 0) currStep++;
				}
				break;
			case 5:
				// Disable tutorial and start game
				SecurePlayerPrefs.SetBool(SavedData.EnabledTutorial, false);
				GameObject.FindObjectOfType<Menu>().RestartGame ();
				currStep++;
				
				GoogleAnalyticsV3.instance.LogEvent(tutorialTag, stageTag, "4-End", 1);
				break;
			default:
				break;
		}
	}
	
	
	void StopMoving() {
		Rigidbody2D[] rigidbodies = GameObject.FindObjectsOfType<Rigidbody2D>();
		foreach (Rigidbody2D rigid in rigidbodies) {
				rigid.velocity = Vector2.zero;
		}
		Generator.instance.moving = false;
		scoreScript.updating = false;
	}
	
	
	void StartMoving() {
		Rigidbody2D[] rigidbodies = GameObject.FindObjectsOfType<Rigidbody2D>();
		foreach (Rigidbody2D rigid in rigidbodies) {
			if (rigid.name != "Boat") {
				rigid.velocity = new Vector2(0, -2.25f);
			}
			if (rigid.name == "Cane") {
				rigid.velocity = new Vector2(0, -3.5f);
			}
		}
		Generator.instance.moving = true;
		scoreScript.updating = true;
	}


	void ShootSilk(Vector2 v) {
		if (characterScript.hasSilk) {
			characterScript.SetMousePos (v);
			characterScript.DestroySilk ();
			characterScript.ShootSilk ();
			characterAnimatorScript.SimulateClick(v);
		}
	}
	
	
	IEnumerator ShowSwipe(Vector2 start, Vector2 end) {
		mouseTrail.transform.position = start;
		mouseTrail.GetComponent<TrailRenderer>().enabled = true;
		Vector2 delta = (end - start)/15;
		while (( (Vector2) mouseTrail.transform.position - end).magnitude > 0.1f) {
			yield return new WaitForEndOfFrame();
			mouseTrail.transform.position += (Vector3)delta;
		}
		
		mouseTrail.GetComponent<TrailRenderer>().enabled = false;
	}
	
	public void EnableDeathOverlay() {
		deathOverlay.SetActive (true);
		if (deathOverlay.GetComponent<CanvasGroup>().alpha == 0)
			deathOverlay.GetComponent<CanvasFader>().FadeIn();
		Text t = deathOverlay.GetComponentInChildren<Text>();

		// Died during video
		if (currStep == 1) {
			t.text = "Oops... that's not supposed to happen. I... I blame your phone! Please try again :)";

			GoogleAnalyticsV3.instance.LogEvent(tutorialTag, deathTag, "Demo", 1);
		}
		else if (elapsedTime < 5) {
			t.text = "Grubby can attach to rocks behind him. Silk will automatically detach when Grubby is aligned to the rock.";
		
			GoogleAnalyticsV3.instance.LogEvent(tutorialTag, deathTag, "1", 1);
		}
		else if (elapsedTime < 11) {
			t.text = "Grubby can only have one silk at a time. Shooting a new silk cuts the old silk.";

			GoogleAnalyticsV3.instance.LogEvent(tutorialTag, deathTag, "2", 1);
		}
		else {
			t.text = "Canes can be shot down with silk. This gives a bonus but destroys the silk.";

			GoogleAnalyticsV3.instance.LogEvent(tutorialTag, deathTag, "3", 1);
		}
	}

	void DisableSlider() {
		slider.SetActive(false);
	}

	public void DemoPlay() {
		currStep++;
		mouseWasUp = false;
		StartMoving ();
		GameObject.Find ("DemoButton").SetActive(false);
		GameObject.Find ("TutorialButton").SetActive(false);

		GoogleAnalyticsV3.instance.LogEvent(tutorialTag, stageTag, "2-Demo Play", 1);
	}
	
	
	public void DemoSkip() {
		boatObject.GetComponent<Boat>().isInvincible = true;
		GameObject.Find ("Video").SetActive(false);
		skipVideo = true;
		Time.timeScale = 10f;
		currStep ++;
		mouseWasUp = false;
		StartMoving ();
		GameObject.Find ("DemoButton").SetActive(false);
		GameObject.Find ("TutorialButton").SetActive(false);
		DisableSlider ();

		GoogleAnalyticsV3.instance.LogEvent(tutorialTag, stageTag, "2-Demo Skip", 1);
	}
	
	
	public void ChangeTimescale(float scale) {
		if (Time.timeScale != scale && !pauseButton.GetComponent<Toggle>().isOn) {
			if (slider.GetComponentInChildren<Text>() != null)
				slider.GetComponentInChildren<Text>().text = "Speed: " + Math.Round (scale, 1).ToString() + "X";
			Time.timeScale = scale;
			Time.fixedDeltaTime = 0.02F * Time.timeScale;
		}
	}


	void PlayVideo() {
		elapsedTime += Time.deltaTime;
		
		if (skipVideo) {
			// Go to next step after a certain period of time
			// (and only if still alive, otherwise goes to last step on death in DEMO)
			if (elapsedTime > 8.8f && boatObject.activeInHierarchy)
				currStep++;
				mouseTrail.GetComponent<TrailRenderer>().enabled = true;
			return;
		}
		
		switch (videoStep) {
			case 0:
				pauseButton.SetActive(false);
				if (elapsedTime > 0.75f) {
					ShootSilk (new Vector2(6.25f, 7.5f));	// Shoot first silk
					videoStep++;
				}
				break;
			case 1:
				if (elapsedTime > 1.4) {
					StartCoroutine(ShowSwipe (new Vector2(4.5f, 7f), new Vector2(7f, 4f)));
					videoStep++;
				}
				break;
			case 2:
				if (elapsedTime > 1.5f) {
					characterScript.DestroySilk();	// Cut first silk
					videoStep++;
				}
				break;
			case 3:
				if (elapsedTime > 2f) {
					ShootSilk (new Vector2(3.5f, 4.5f));	// Shoot behind
					videoStep++;
				}
				break;
			case 4:
				if (elapsedTime > 2.9f) {
					ShootSilk (new Vector2(4.75f, 8.5f));	// Shoot silk far right dodge cane
					videoStep++;
				}
				break;
			case 5:
				if (elapsedTime > 3.6f) {
					ShootSilk (new Vector2(3f, 4.5f));
					videoStep++;
				}
				break;
			case 6:
				if (elapsedTime > 4.4f) {
					ShootSilk (new Vector2(6f, 9f));
					videoStep++;
				}
				break;
			case 7:
				if (elapsedTime > 5.3f) {
					StartCoroutine(ShowSwipe (new Vector2(4f, 8f), new Vector2(7f, 5f)));
					videoStep++;
				}
				break;
			case 8:
				if (elapsedTime > 5.4f) {
					characterScript.DestroySilk();	// Cut second silk
					videoStep++;
				}
				break;
			case 9:
				if (elapsedTime > 6f) {
					ShootSilk (new Vector2(5f, 9f));	// Shoot cane 2
					videoStep++;
				}
				break;
			case 10:
				if (elapsedTime > 7.4f) {
					ShootSilk (new Vector2(3.5f, 3f));	// Shoot last rock
					videoStep++;
				}
				break;
			case 11:
				// Go to next step after a certain period of time
				// (and only if still alive, otherwise goes to last step on death in DEMO)
				if (elapsedTime > 8.8f && boatObject.activeInHierarchy) {
					pauseButton.SetActive(true);
					mouseTrail.GetComponent<TrailRenderer>().enabled = true;
					currStep++;
				}
				break;
			default:
				break;
		}
	}	
	
	bool CloseToRock(float dist) {
		dist -= 1; // offset by 1 to avoid raycasting self
		//		Debug.DrawRay ((Vector2) boatObject.transform.position + Vector2.up,
		//		                (Vector2) Vector2.up * dist);
		return Physics2D.Raycast((Vector2) boatObject.transform.position + Vector2.up,
		                         (Vector2) Vector2.up, dist).collider != null;
	}
}
