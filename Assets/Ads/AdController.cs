﻿using UnityEngine;
using System.Collections;
using ChartboostSDK;
using GoogleMobileAds.Api;

public class AdController : MonoBehaviour {

	int gameCounter;
	int gameCounterInit = 5;

	static InterstitialAd admobInterstitial;


	// Use this for initialization
	void Start () {
		gameCounter = gameCounterInit;

		AdColony.Configure( "1.0", "appc2362511050545d1b0", "vz280a07c0225e452ba3" );
		InitAdmobInterstitial();
		RequestInterstitial();
	}

	void InitAdmobInterstitial() {
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-1416571493432562/3321917138";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Initialize an InterstitialAd.
		admobInterstitial = new InterstitialAd(adUnitId);
	}

	void OnLevelWasLoaded(int level) {
		bool enabledAds = SecurePlayerPrefs.GetBool(SavedData.EnabledAds);
		if (Application.loadedLevelName == "Game" && enabledAds) {
			RequestInterstitial();
			gameCounter--;
			if (gameCounter == 0) {
				gameCounter = gameCounterInit;
				ShowInterstitial();
			}
		}
	}

	public static void ShowInterstitial() {
		if (Chartboost.hasInterstitial(CBLocation.GameOver)) {
			Chartboost.showInterstitial(CBLocation.GameOver);
		}
		else if (admobInterstitial.IsLoaded()) {
			admobInterstitial.Show();
		}
		else {
			Debug.LogWarning("Interstitial failed to load.");
		}
	}

	void RequestInterstitial() {
		RequestChartboostInterstitial();
		RequestAdmobInterstitial();
	}

	void RequestChartboostInterstitial() {
		if (!Chartboost.hasInterstitial(CBLocation.GameOver)) {
			Chartboost.cacheInterstitial(CBLocation.GameOver);
		}
	}

	void RequestAdmobInterstitial() {
		if (!admobInterstitial.IsLoaded()) {
			// Create an empty ad request.
			AdRequest request = new AdRequest.Builder().Build();
			// Load the interstitial with the request.
			admobInterstitial.LoadAd(request);
		}
	}
}
