﻿using UnityEngine;
using System.Collections;

public class ScreenShaker : MonoBehaviour {

	float shake = 0;
	float shakeAmount = 0.25f;
	float dampingFactor = 10f;

	Vector3 originalPos;

	// Update is called once per frame
	void Update () {
		if (shake > 0) {
			Camera.main.transform.position = originalPos + Random.insideUnitSphere * shakeAmount;
			shake -= Time.deltaTime * dampingFactor;
			
		} else {
			Camera.main.transform.position = originalPos;
			shake = 0.0f;
		}
	}

	public void Shake() {
		shake = 1f;
	}

	public void SetOriginalPosition(Vector3 pos) {
		originalPos = pos;
	}
}
