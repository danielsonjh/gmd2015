﻿using UnityEngine;
using System.Collections;

public class Scaler : MonoBehaviour {

	Camera theCamera;

	// Use this for initialization
	void Start () {
		theCamera = GetComponent<Camera>();
		if (ScreenIsTall ()) {
			theCamera.orthographicSize = 3.5f / theCamera.aspect;
		}
//		// Wide screens (PC)
//		else {
//			int maxHeight = 0;
//			Resolution[] res = Screen.resolutions;
//			foreach (Resolution r in res) {
//				if (r.height > maxHeight) {
//					maxHeight = r.height;
//				}
//			}
//			Screen.SetResolution((int)(maxHeight * 9f/16f), maxHeight, false);
//			camera.aspect = 9f/16f;
//			camera.orthographicSize = 3.5f / GetComponent<Camera>().aspect;
//			camera.orthographicSize = 16f/9f * 3.5f;
//		}
		theCamera.transform.position = new Vector3(3.5f, GetComponent<Camera>().orthographicSize, -10f);
		GetComponent<ScreenShaker>().SetOriginalPosition(theCamera.transform.position);
	}
	
	bool ScreenIsTall() {
		return theCamera.pixelHeight > theCamera.pixelWidth;
	}
}
