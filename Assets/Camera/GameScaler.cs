﻿using UnityEngine;
using System.Collections;

public class GameScaler : MonoBehaviour {

	float widescreenAspect = 9f / 16f;
	float widescreenGrubbusHeight = 11f;

	// Use this for initialization
	void Start () {
		if (Application.loadedLevelName != "Game")
			return;

		float heightScalingFactor = widescreenAspect / Camera.main.aspect;

		GameObject boat = GameObject.Find("Boat");
		Vector2 boatPos = boat.transform.position;
		boat.transform.position = new Vector2(boatPos.x, boatPos.y * heightScalingFactor);

		Grubbus grubbusScript = FindObjectOfType<Grubbus>();
		grubbusScript.SetPosition(widescreenGrubbusHeight * heightScalingFactor);
	}
}
