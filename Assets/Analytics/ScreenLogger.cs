﻿using UnityEngine;
using System.Collections;

public class ScreenLogger : MonoBehaviour {

	void OnLevelWasLoaded(int level) {
		if (GoogleAnalyticsV3.instance) {
			GoogleAnalyticsV3.instance.LogScreen(Application.loadedLevelName);
			GoogleAnalyticsV3.instance.DispatchHits();
		}
	}
}
