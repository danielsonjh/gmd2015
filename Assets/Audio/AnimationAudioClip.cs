﻿using UnityEngine;
using System.Collections;

public class AnimationAudioClip : MonoBehaviour {

	public AudioClip theClip;
	
	void OnEnable () {
		AudioManager.instance.PlayClip(theClip);
	}
}
