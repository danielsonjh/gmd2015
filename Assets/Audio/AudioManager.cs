﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {

	Dictionary<string, AudioSource> audioSourceDict;
	string volumeBGM = "VolumeBGM";
	string volumeSFX = "VolumeSFX";

	public AudioMixer mixer;

	public AudioMixerSnapshot unpausedSnapshot;
	public AudioMixerSnapshot pausedSnapshot;

	public AudioClip BGM;
	public AudioClip button;
	public AudioClip splash;
	public AudioClip cane;
	public AudioClip death;
	public AudioClip bonus;
	public AudioClip silkCut;
	public AudioClip silkShoot;

	
	public static AudioManager instance;

	void Awake () {
		if(instance)
			DestroyImmediate(gameObject);
		else {
			DontDestroyOnLoad(gameObject);
			instance = this;
		}
	}

	// Use this for initialization
	void Start () {
		InitAudioSourceDict();
	}

	void InitAudioSourceDict() {
		audioSourceDict = new Dictionary<string, AudioSource>();
		AudioSource[] audioSources = GetComponents<AudioSource>();
		foreach (AudioSource audioSource in audioSources) {
			audioSourceDict.Add(audioSource.clip.name, audioSource);
		}
	}
	
	public void PlayClip(AudioClip clip) {
		string clipName = clip.name;
		if (!audioSourceDict.ContainsKey(clipName))
			Debug.LogError(clipName + " does not have an associated AudioSource in AudioManager.");
		audioSourceDict[clipName].Play();
	}

	public bool IsPlayingClip(AudioClip clip) {
		return audioSourceDict[clip.name].isPlaying;
	}

	public void ToggleBGM(bool isOn) {
		if (isOn) {
			mixer.ClearFloat(volumeBGM);
			PlayClip(BGM);
		}
		else {
             mixer.SetFloat(volumeBGM, -80f);	// Volume in dB
		}
	}

	public void ToggleSFX(bool isOn) {
		if (isOn)
			mixer.ClearFloat(volumeSFX);
		else
			mixer.SetFloat(volumeSFX, -80f);	// Volume in dB
	}

	float ConvertVolumePercentageToDB(float percentage) {
		// Lowest volume is -80dB, so it's considered to be '0' volume
		return 80 * percentage - 80;
	}
}
